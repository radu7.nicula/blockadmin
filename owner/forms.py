from django import forms
from django.forms import TextInput, Select

from administrators.models import Counter
from owner.models import ReadIndex


class ReadIndexForm(forms.ModelForm):
    class Meta:
        model = ReadIndex
        fields = ['tip_counter', 'current_index', 'read_date', 'read_by']
        widgets = {
            'tip_counter': Select(attrs={'class': 'form-control'}),
            'current_index': TextInput(attrs={'placeholder': 'indexul curent', 'class': 'form-control'}),
            'read_date': TextInput(attrs={'type': 'date', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        self.read_by = kwargs['initial']['read_by']
        super(ReadIndexForm, self).__init__(*args, **kwargs)
        # self.fields['counter'].queryset = Counter.objects.filter(id=counter)
        # self.initial['counter'] = Counter.objects.filter(id=counter).last()

    def save(self, commit=True):
        obj = super(ReadIndexForm, self).save(False)
        obj.read_by = self.read_by
        commit and obj.save()
        return obj
