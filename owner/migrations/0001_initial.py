# Generated by Django 3.2.4 on 2021-07-30 15:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('administrators', '0006_alter_apartment_apartment_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReadIndex',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('current_index', models.IntegerField()),
                ('read_date', models.DateField()),
                ('counter', models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='administrators.counter')),
            ],
        ),
    ]
