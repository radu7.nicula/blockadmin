from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from owner import views
from owner.views import PostJsonListView, MainView

urlpatterns = [
    path('meter_readings/', views.ReadIndexView.as_view(), name='meter_readings'),
    path('select/', views.select_apartment, name='select'),
    path('select/<int:pk>', views.get_apartments_per_user, name='select_apart'),
    path('owner_cost/', views.OwnerCostsList.as_view(), name='owner_cost'),
    path('history_cost/', views.pie_chart, name='history_cost'),
    # path('display_reads/', views.DisplayReads.as_view(), name='display_reads'),
    path('', MainView.as_view(), name='main-view'),
    path('posts-json/<int:num_posts>/', PostJsonListView.as_view(), name='posts-json-view'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

