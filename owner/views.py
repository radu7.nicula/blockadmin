from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import CreateView, ListView, TemplateView

from costs.models import Costs
from owner.forms import ReadIndexForm
from administrators.models import Apartment
from owner.models import ReadIndex


def select_apartment(request):
    return render(request, 'owner/select_apartment.html')


def get_apartments_per_user(request):
    all_apartments = Apartment.objects.filter()
    context = {'all_apartments': all_apartments}
    return render(request, 'owner/select_apartment.html', context)


class ReadIndexView(CreateView):
    template_name = 'owner/meter_readings.html'
    form_class = ReadIndexForm
    success_url = reverse_lazy('')

    # def get_form_kwargs(self, *args, **kwargs):
    #     counter = Counter.objects.all().filter(owner=request.user)
    #     apart = counter.apartamnet
    #     # id_counter = self.request.GET.get('id')
    #     kwargs = super(ReadIndexView, self).get_form_kwargs()
    #     kwargs.update({'counter': counter})
    #     return kwargs

    def get_initial(self):
        self.initial.update({'read_by': self.request.user})
        return self.initial


class OwnerCostsList(ListView):
    template_name = 'owner/owner_costs.html'
    model = Costs
    context_object_name = 'all_costs'


# class DisplayReads(ListView):
#     template_name = 'costs/reads_display.html'
#     model = ReadIndex
#     context_object_name = 'all_reads'

class MainView(TemplateView):
    template_name = 'costs/reads_display.html'


class PostJsonListView(View):
    def get(self, *args, **kwargs):
        print(kwargs)
        upper = kwargs.get('num_posts')
        lower = upper - 3
        posts = list(ReadIndex.objects.values()[lower:upper])
        posts_size = len(ReadIndex.objects.all())
        max_size = True if upper >= posts_size else False
        return JsonResponse({'data': posts, 'max': max_size}, safe=False)


def pie_chart(request):
    labels = []
    data = []

    queryset = Costs.objects.filter(apartment_id=3)
    for cost in queryset:
        labels.append(cost.tip_cost)
        data.append(cost.value())

    return render(request, 'owner/history_costs.html', {
        'labels': labels,
        'data': data,
    })


