import datetime

from django.contrib.auth.models import User
from django.db import models

from administrators.models import Apartment, Counter


class ReadIndex(models.Model):
    TIP_COUNTER = (
        ('Apa Rece', 'Apa Rece'),
        ('Apa Calda', 'Apa Calda'),
        ('Curent Electric', 'Curent Electric'),
    )
    read_by = models.ForeignKey(User, on_delete=models.CASCADE)
    current_index = models.IntegerField()
    read_date = models.DateTimeField(default=datetime.date.today)
    tip_counter = models.CharField(max_length=100, choices=TIP_COUNTER, default=TIP_COUNTER[0])

    def __str__(self):
        return str(self.pk)
