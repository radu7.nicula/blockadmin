from django import forms
from django.contrib.auth.models import User
from django.forms import ModelChoiceField

from administrators.models import Association, Counter, Apartment, Block


class AddAssociationForm(forms.ModelForm):
    class Meta:
        model = Association
        fields = '__all__'

    def __init__(self, username, *args, **kwargs):
        super(AddAssociationForm, self).__init__(*args, **kwargs)
        self.username = username
        self.fields['administrator'] = ModelChoiceField(queryset=User.objects.all())
        self.initial['administrator'] = User.objects.get(username=username)
        self.fields['administrator'].widget = forms.HiddenInput()
        self.fields['administrator'].label = ''

    def clean_title(self):
        association_name = self.cleaned_data['association_name']
        if Association.objects.filter(user=self.user, association_name=association_name).exists():
            raise forms.ValidationError("Aveti deja o asociatie cu acest nume")
        return association_name


class AddCounterForm(forms.ModelForm):
    class Meta:
        model = Counter
        fields = '__all__'

    def __init__(self, apartment, *args, **kwargs):
        super(AddCounterForm, self).__init__(*args, **kwargs)
        self.fields['apartment'].queryset = Apartment.objects.filter(id=apartment)
        self.initial['apartment'] = Apartment.objects.filter(id=apartment).last()
        self.fields['apartment'].widget = forms.HiddenInput()
        self.fields['apartment'].label = ''


class AddBlockForm(forms.ModelForm):
    class Meta:
        model = Block
        fields = '__all__'

    def __init__(self, association, *args, **kwargs):
        super(AddBlockForm, self).__init__(*args, **kwargs)
        self.fields['association'].queryset = Association.objects.filter(id=association)
        self.initial['association'] = Association.objects.filter(id=association).last()
        self.fields['association'].widget = forms.HiddenInput()
        self.fields['association'].label = ''


class AddApartmentForm(forms.ModelForm):
    class Meta:
        model = Apartment
        fields = '__all__'

    def __init__(self, block, *args, **kwargs):
        super(AddApartmentForm, self).__init__(*args, **kwargs)
        self.fields['block'].queryset = Block.objects.filter(id=block)
        self.initial['block'] = Block.objects.filter(id=block).last()
        self.fields['block'].widget = forms.HiddenInput()
        self.fields['block'].label = ''
