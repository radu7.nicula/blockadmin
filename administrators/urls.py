from django.urls import path

from administrators import views

urlpatterns = [

    path('adaugare/', views.adaugare, name='adaugare'),


    # Add Views

    path('add_association/', views.AddAssociationView.as_view(), name='add_association'),
    path('add_block/', views.AddBlockView.as_view(), name='add_block'),
    path('add_apartment/', views.AddApartmentView.as_view(), name='add_apartment'),
    path('add_counter/', views.AddCounterView.as_view(), name='add_counter'),

    # Update Views

    path('edit_block/<int:pk>/', views.EditBlockView.as_view(), name='edit_block'),
    path('edit_association/<int:pk>/', views.EditAssociationView.as_view(), name='edit_association'),
    path('edit_apartament/<int:pk>/', views.EditApartmentView.as_view(), name='edit_apartment'),
    path('edit_counter/<int:pk>/', views.EditCounterView.as_view(), name='edit_counter'),

    # Delete Views

    path('delete_block/<int:pk>/', views.DeleteBlockView.as_view(), name='delete_block'),
    path('delete_association/<int:pk>/', views.DeleteAssociationView.as_view(), name='delete_association'),
    path('delete_apartment/<int:pk>/', views.DeleteApartmentBlockView.as_view(), name='delete_apartment'),
    path('delete_counter/<int:pk>/', views.DeleteCounterView.as_view(), name='delete_counter'),

    # Detail Views

    path('block_details/<int:pk>/', views.BlockDetailView.as_view(), name='block_details'),
    path('association_details/<int:pk>/', views.AssociationDetailView.as_view(), name='association_details'),
    path('apartment_details/<int:pk>/', views.ApartmentDetailView.as_view(), name='apartment_details'),
    path('counter_details/<int:pk>/', views.CounterDetailView.as_view(), name='counter_details'),

    # List Views

    path('association_list/', views.AssociationListView.as_view(), name='association_list'),
    path('blocks_list/<int:pk>/', views.BlockListView.as_view(), name='blocks_list'),
    path('apartments_list/<int:pk>/', views.ApartmentListView.as_view(), name='apartment_list'),
    path('counters_list/<int:pk>/', views.CountersListView.as_view(), name='counters_list'),
]
