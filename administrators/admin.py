from django.contrib import admin

# Register your models here.
from administrators.models import Association, Block, Apartment, Counter

admin.site.register(Association)
admin.site.register(Block)
admin.site.register(Apartment)
admin.site.register(Counter)
