from django.contrib.auth.models import User
from django.urls import reverse

import users.models
from django.db import models


class Association(models.Model):
    association_name = models.CharField(max_length=256)
    address = models.CharField(max_length=256)
    administrator = models.ForeignKey(users.models.User, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.association_name

    def get_absolute_url(self):
        return reverse('association_details', args=[self.id])


class Block(models.Model):
    block_number = models.IntegerField()
    number_of_floors = models.IntegerField()
    is_elevator = models.BooleanField()
    number_of_apartments = models.IntegerField()
    association = models.ForeignKey(Association, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.block_number)


class Apartment(models.Model):
    apartment_number = models.CharField(max_length=10)
    floor = models.CharField(max_length=10)
    owner = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    block = models.ForeignKey(Block, default='', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.apartment_number) + ' ' + str(self.block.association)


class Counter(models.Model):
    apartment = models.ForeignKey(Apartment, default='', on_delete=models.CASCADE)
    index = models.IntegerField()
    tip_counter = models.CharField(max_length=256)

    def __str__(self):
        return str(self.tip_counter)



