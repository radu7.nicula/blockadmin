import django_filters

from administrators.models import Association


class AssociationFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='iexact')

    class Meta:
        model = Association
        fields = '__all__'
