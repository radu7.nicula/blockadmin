from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView

from administrators.filters import AssociationFilter
from administrators.forms import AddAssociationForm, AddCounterForm, AddBlockForm, AddApartmentForm
from administrators.models import Block, Association, Apartment, Counter


def adaugare(request):
    return render(request, 'administrators/adaugare.html')


def home(request):
    return render(request, 'base.html')


class AddBlockView(CreateView):
    template_name = 'administrators/add_block.html'
    form_class = AddBlockForm
    success_url = reverse_lazy('add_block')

    def get_form_kwargs(self, *args, **kwargs):
        id_assoc = self.request.GET.get('id')
        kwargs = super(AddBlockView, self).get_form_kwargs()
        kwargs.update({'association': id_assoc})
        return kwargs


class AddCounterView(CreateView):
    template_name = 'administrators/add_counter.html'
    form_class = AddCounterForm
    success_url = reverse_lazy('add_counter')

    def get_form_kwargs(self, *args, **kwargs):
        id_apart = self.request.GET.get('id')
        kwargs = super(AddCounterView, self).get_form_kwargs()
        kwargs.update({'apartment': id_apart})

        return kwargs


class AddAssociationView(CreateView):
    template_name = 'administrators/add_association.html'
    form_class = AddAssociationForm
    success_url = reverse_lazy('home')

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(AddAssociationView, self).get_form_kwargs()
        kwargs.update({'username': self.request.user.username})
        return kwargs


class AddApartmentView(CreateView):
    template_name = 'administrators/add_apartment.html'
    form_class = AddApartmentForm
    success_url = reverse_lazy('add_apartment')

    def get_form_kwargs(self, *args, **kwargs):
        id_block = self.request.GET.get('id')
        kwargs = super(AddApartmentView, self).get_form_kwargs()
        kwargs.update({'block': id_block})
        return kwargs


def get_blocks_per_admin(request, pk):
    all_blocks = Block.objects.filter(admin_id=pk)
    context = {
        'all_blocks_per_admin': all_blocks
    }
    return render(request, 'home/home_page.html', context)


def get_associations_per_user(request, pk):
    all_associations = Association.objects.filter(administrator_id=pk)
    context = {'all_associations': all_associations}
    return render(request, 'home/home_page.html', context)


class BlockListView(ListView):
    template_name = 'administrators/blocks_list.html'
    model = Block
    fields = '__all__'
    context_object_name = 'all_blocks'

    def get_context_data(self, *args, **kwargs):
        return super().get_context_data(
            *args,
            pk=self.kwargs['pk'],
            **kwargs
        )


class ApartmentListView(ListView):
    template_name = 'administrators/apartments_list.html'
    model = Apartment
    fields = '__all__'
    context_object_name = 'all_apartments'

    def get_context_data(self, *args, **kwargs):
        return super().get_context_data(
            *args,
            pk=self.kwargs['pk'],
            **kwargs
        )


class CountersListView(ListView):
    template_name = 'administrators/counters_list.html'
    model = Counter
    fields = '__all__'
    context_object_name = 'all_counters'

    def get_context_data(self, *args, **kwargs):
        return super().get_context_data(*args, pk=self.kwargs['pk'], **kwargs)


class AssociationListView(ListView):
    template_name = 'administrators/association_list.html'

    model = Association
    fields = '__all__'
    context_object_name = 'all_associations'

    # def get_queryset(self):
    #     qs = self.model.objects.all()
    #     association_filtered_list = AssociationFilter(self.request.GET, queryset=qs)
    #     return association_filtered_list
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = AssociationFilter(self.request.GET, queryset=self.get_queryset())
        return context


class EditBlockView(UpdateView):
    template_name = 'administrators/edit_block.html'
    model = Block
    fields = '__all__'
    success_url = reverse_lazy('blocks_list')


class EditAssociationView(UpdateView):
    template_name = 'administrators/edit_association.html'
    model = Association
    fields = '__all__'
    success_url = reverse_lazy('association_list')


class EditApartmentView(UpdateView):
    template_name = 'administrators/edit_apartment.html'
    model = Apartment
    fields = '__all__'
    success_url = reverse_lazy('apartment_list')


class EditCounterView(UpdateView):
    template_name = 'administrators/edit_counter.html'
    model = Counter
    fields = '__all__'
    success_url = reverse_lazy('counters_list')


class DeleteBlockView(DeleteView):
    template_name = 'administrators/delete_block.html'
    model = Block
    success_url = reverse_lazy('blocks_list')


class DeleteAssociationView(DeleteView):
    template_name = 'administrators/delete_association.html'
    model = Association
    success_url = reverse_lazy('association_list')


class DeleteApartmentBlockView(DeleteView):
    template_name = 'administrators/delete_apartment.html'
    model = Apartment
    success_url = reverse_lazy('apartment_list')


class DeleteCounterView(DeleteView):
    template_name = 'administrators/delete_counter.html'
    model = Counter
    success_url = reverse_lazy('counters_list')


class BlockDetailView(DetailView):
    template_name = 'administrators/block_detail.html'
    model = Block
    success_url = reverse_lazy('blocks_list')


class AssociationDetailView(DetailView):
    template_name = 'administrators/association_details.html'
    model = Association
    success_url = reverse_lazy('association_list')


class ApartmentDetailView(DetailView):
    template_name = 'administrators/apartment_details.html'
    model = Apartment
    success_url = reverse_lazy('apartment_list')


class CounterDetailView(DetailView):
    template_name = 'administrators/counter_details.html'
    model = Counter
    success_url = reverse_lazy('counters_list')
