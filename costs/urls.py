from django.urls import path

from costs.views import CostCreateView, CostListView, DeleteCostView, CostUpdateView, CostDetailView

urlpatterns = [
    path('create_cost/', CostCreateView.as_view(), name='add_cost'),
    path('costs_list/<int:pk>/', CostListView.as_view(), name='costs_list'),
    path('delete_cost/<int:pk>/', DeleteCostView.as_view(), name='delete_cost'),
    path('update_cost/<int:pk>/', CostUpdateView.as_view(), name='update_cost'),
    path('cost_details/<int:pk>/', CostDetailView.as_view(), name='cost_details'),
]
