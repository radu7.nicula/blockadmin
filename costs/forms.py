from django import forms
from django.forms import ModelForm, TextInput
from django.forms.widgets import Select

from administrators.models import Apartment
from costs.models import Costs


class AddCostsForm(ModelForm):
    class Meta:
        model = Costs
        fields = '__all__'
        widgets = {
            'cost_date': TextInput(attrs={'type': 'date', 'class': 'form-control'}),
            'tip_cost': Select(attrs={'placeholder': 'select', 'class': 'form-control'}),
            'quantity': TextInput(attrs={'placeholder': 'quantity', 'class': 'form-control'}),
            'cost_per_unit': TextInput(attrs={'class': 'form-control'}),
            'cost_division': Select(attrs={'class': 'form-control'}),
        }

    def __init__(self, apartment, *args, **kwargs):
        super(AddCostsForm, self).__init__(*args, **kwargs)
        self.fields['apartment'].queryset = Apartment.objects.filter(apartment_number=apartment)
        self.initial['apartment'] = Apartment.objects.filter(apartment_number=apartment).last()
        self.fields['apartment'].widget = forms.HiddenInput()
        self.fields['apartment'].label = ''
