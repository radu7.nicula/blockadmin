from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DeleteView, DetailView, UpdateView

from costs.forms import AddCostsForm
from costs.models import Costs


class CostCreateView(CreateView):
    template_name = 'costs/add_cost.html'
    form_class = AddCostsForm
    success_url = reverse_lazy('apartment_list/pk')
    context_object_name = 'all_costs'

    def get_form_kwargs(self, *args, **kwargs):
        id_apart = self.request.GET.get('id')
        kwargs = super(CostCreateView, self).get_form_kwargs()
        kwargs.update({'apartment': id_apart})

        return kwargs


class CostListView(ListView):
    template_name = 'costs/costs_list.html'
    model = Costs
    fields = '__all__'
    context_object_name = 'all_costs'


class DeleteCostView(DeleteView):
    template_name = 'costs/delete_cost.html'
    model = Costs
    # success_url = reverse_lazy('')


class CostDetailView(DetailView):
    template_name = 'costs/cost_detail.html'
    model = Costs
    context_object_name = 'all_costs'


class CostUpdateView(UpdateView):
    template_name = 'costs/cost_update.html'
    model = Costs
    context_object_name = 'all_costs'
