from django.db import models
import administrators.models


class Costs(models.Model):
    COST_CHOICES = (
        ('spatiu_verde', 'Mentenanta spatiu verde'),
        ('apa_rece', 'Apa rece'),
        ('apa_calda', 'Apa calda'),
        ('curatenie_scara', 'Curatenie scara'),
    )

    COST_DIVISION = (
        ('contorizat', 'Contorizat'),
        ('per_apart', 'Pe apartament'),
    )
    tip_cost = models.CharField(max_length=265, choices=COST_CHOICES)
    cost_division = models.CharField(max_length=256, choices=COST_DIVISION)
    cost_per_unit = models.IntegerField()
    quantity = models.IntegerField()
    cost_date = models.DateField()
    apartment = models.ForeignKey(administrators.models.Apartment, default="", on_delete=models.CASCADE)

    def value(self):
        return self.cost_per_unit * self.quantity

    def __str__(self):
        return self.tip_cost
