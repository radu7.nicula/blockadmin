# Generated by Django 3.2.4 on 2021-07-22 15:38

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Costs',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cost_name', models.CharField(max_length=265)),
                ('cost_division', models.CharField(max_length=256)),
                ('cost_per_unit', models.IntegerField()),
                ('quantity', models.IntegerField()),
            ],
        ),
    ]
