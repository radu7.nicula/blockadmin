from administrators.models import Association, Block, Apartment


def get_all_associations(request):
    all_associations = Association.objects.all()
    return {'all_associations': all_associations}


def get_all_blocks(request):
    all_blocks = Block.objects.all()
    return {'all_blocks': all_blocks}


def get_all_apartments(request):
    all_apartments = Apartment.objects.all()
    return {'all_apartments': all_apartments}
