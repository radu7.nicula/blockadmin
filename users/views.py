from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views.generic import CreateView

from users.forms import UserAdministratorForm, UserOwnerForm


class UserAdminCreateView(CreateView):
    template_name = 'users/create_admin_user.html'
    model = User
    form_class = UserAdministratorForm
    success_url = reverse_lazy('home')


class UserOwnerCreateView(CreateView):
    template_name = 'users/create_owner_user.html'
    model = User
    form_class = UserOwnerForm
    success_url = reverse_lazy('home')
