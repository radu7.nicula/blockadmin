from django.contrib.auth.models import User, AbstractUser
from django.db import models


class Administrators(User):
    phone = models.CharField(max_length=50)
    is_staff = True
    is_active = False


class Owners(User):
    phone = models.CharField(max_length=50)
    is_staff = False
    is_active = True

