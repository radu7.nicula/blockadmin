from django.contrib.auth.forms import UserCreationForm
from django.forms import TextInput
from users.models import Administrators, Owners


class UserAdministratorForm(UserCreationForm):
    class Meta:
        model = Administrators
        fields = ['username', 'first_name', 'last_name', 'phone', 'email', 'is_staff']
        widgets = {
            'username': TextInput(attrs={'placeholder': 'user name', 'class': 'form-control'}),
            'first_name': TextInput(attrs={'placeholder': 'first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'last name', 'class': 'form-control'}),
            'phone': TextInput(attrs={'placeholder': 'phone', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'email', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(UserAdministratorForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['placeholder'] = 'password'
        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['placeholder'] = 'confirm password'


class UserOwnerForm(UserCreationForm):
    class Meta:
        model = Owners
        fields = ['username', 'first_name', 'last_name', 'phone', 'email', 'is_active']
        widgets = {
            'username': TextInput(attrs={'placeholder': 'user name', 'class': 'form-control'}),
            'first_name': TextInput(attrs={'placeholder': 'first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'last name', 'class': 'form-control'}),
            'phone': TextInput(attrs={'placeholder': 'phone', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'email', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(UserOwnerForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['placeholder'] = 'password'
        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['placeholder'] = 'confirm password'
