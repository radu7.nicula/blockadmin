from django.urls import path

from users import views

urlpatterns = [
    path('create_user/', views.UserAdminCreateView.as_view(), name='create-user'),
    path('create_user/', views.UserOwnerCreateView.as_view(), name='create_owner_user'),
]
