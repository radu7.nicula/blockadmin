from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DeleteView, DetailView

from contact.forms import ContactForm
from contact.models import Contact


def success(request):
    return render(request, 'contact/success.html')


class ContactCreateView(CreateView):
    template_name = 'contact/contact.html'
    form_class = ContactForm
    success_url = reverse_lazy('success')


class ContactListView(ListView):
    template_name = 'contact/messages.html'
    model = Contact
    fields = '__all__'
    context_object_name = 'all_messages'


class DeleteMessageView(DeleteView):
    template_name = 'contact/delete_message.html'
    model = Contact
    success_url = reverse_lazy('messages')


class MessageDetailView(DetailView):
    template_name = 'contact/message_detail.html'
    model = Contact
    context_object_name = 'message'
