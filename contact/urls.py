from django.urls import path

from contact.views import ContactCreateView, success, ContactListView, DeleteMessageView, MessageDetailView

urlpatterns = [
    path('', ContactCreateView.as_view(), name='contact'),
    path('success/', success, name='success'),
    path('messages/', ContactListView.as_view(), name='messages'),
    path('delete_message/<int:pk>/', DeleteMessageView.as_view(), name='delete_message'),
    path('message_details/<int:pk>/', MessageDetailView.as_view(), name='message_details'),
]
